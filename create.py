import requests
import getpass

# Замените 'YOUR_GITLAB_API_TOKEN' и 'YOUR_GITLAB_URL' на ваши данные
gitlab_token = getpass.getpass("Введите ваш токен доступа GitLab (без отображения): ")
gitlab_url = input("YOUR_GITLAB_URL: ")

project_name = "{{cookiecutter.repo_name}}"
# project_description = 'My GitLab project'

headers = {
    'Private-Token': gitlab_token,
}

data = {
    'name': project_name,
}

create_project_url = f'{gitlab_url}/api/v4/projects'
response = requests.post(create_project_url, headers=headers, data=data)

if response.status_code == 201:
    print(f'Project {project_name} created successfully.')
else:
    print('Failed to create project. Check your GitLab API token and URL.')
